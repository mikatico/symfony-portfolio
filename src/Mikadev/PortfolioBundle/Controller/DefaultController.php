<?php

namespace Mikadev\PortfolioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('MikadevPortfolioBundle:Default:index.html.twig');
    }

    public function contentAction($name)
    {
    	$content = $this->getDoctrine()
                        ->getManager()
    					->getRepository('MikadevPortfolioBundle:Page')
                        ->findOneByName($name);

        $id = $content[0];

        $images = $this->getDoctrine()
                       ->getManager()
                       ->getRepository('MikadevPortfolioBundle:Image')
                       ->findImagesByidPage($id);

        if (!$content) {
            throw $this->createNotFoundException('La page n\'existe pas, ou n\'est pas accessible !');
        }

    	return $this->render('MikadevPortfolioBundle:Default:content.html.twig', array('content' => $content[0], 'images' => $images));
    }

    public function projetAction()
    {
        $projets = $this->getDoctrine()
                        ->getManager()
                        ->getRepository('MikadevPortfolioBundle:Page')
                        ->findAllNameTitreLogoDescriptionByType('projet');

        if (!$projets) {
            throw $this->createNotFoundException("Erreur interne !");
        }

        return $this->render('MikadevPortfolioBundle:Default:projet.html.twig', array('projets' => $projets));
    }

    public function stageAction()
    {
        $stages = $this->getDoctrine()
                       ->getManager()
                       ->getRepository('MikadevPortfolioBundle:Page')
                       ->findAllNameTitreLogoDescriptionByType('stage');    

        if (!$stages) {
            throw $this->createNotFoundException("Erreur interne");
        }

        return $this->render('MikadevPortfolioBundle:Default:stage.html.twig', array('stages' => $stages));
    }

    public function cvAction()
    {
        return $this->render('MikadevPortfolioBundle:Default:cv.html.twig');
    }

    public function identiteAction()
    {
        return $this->render('MikadevPortfolioBundle:Default:identite.html.twig');
    }

    public function rssAction()
    {
        $link = array(
            'fermeduweb' => 'http://feeds.lafermeduweb.net/LaFermeDuWeb',
            'korben' => 'http://feeds.feedburner.com/KorbensBlog-UpgradeYourMind?format=xml',
            'grafikart' => 'http://feeds.feedburner.com/Grafikart',
            'begeek' => 'http://www.begeek.fr/feed',
        );

        $contents = array(

        );

        $titres = array(

        );

        $cpt = 0;
        $indice = 0;
        foreach ($link as $key => $value) {

            $rss = simplexml_load_file($value);
            $titres[$indice] = $key;
            foreach ($rss->channel->item as $item) {
                $datetime = date_create($item->pubDate);
                $date = date_format($datetime, 'd/M/Y H:i');
                if ($cpt > 4) {
                    $cpt = 0;
                    break;
                }
                $contents[$indice]['content'][] = array(
                    'link' => $item->link,
                    'title' => $item->title,
                    'date' => $date,
                );
                $cpt++;
            }
            $indice++;
        }

        return $this->render('MikadevPortfolioBundle:Default:rss.html.twig', array('contents' => $contents, 'titles' => $titres));
    }

    public function oopsAction()
    {
        return $this->render('MikadevPortfolioBundle:Default:oops.html.twig');
    }
}
