<?php

namespace Mikadev\PortfolioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Mikadev\PortfolioBundle\Entity\Page;

class ContentController extends Controller
{
    public function indexAction()
    {   
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('oops'));
        }

        $repository = $this->getDoctrine()
                   ->getManager()
                   ->getRepository('MikadevPortfolioBundle:Page');

        $contents = $repository->findAllByType();
        $nbContent = $repository->findCount();
          
        return $this->render('MikadevPortfolioBundle:Content:index.html.twig', 
            array( 'contents' => $contents, 'nbContent' => $nbContent['0']['1']));
    }

    public function addAction(Request $request)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('oops'));
        }

    	$page = new Page();
    	$page->updatedTimestamps();

		$form = $this->createFormBuilder($page)
	        ->add('name', 'text')
            ->add('titre', 'text')
            ->add('soustitre', 'text')
	        ->add('content', 'textarea', array('required' => false))
	        ->add('logo', 'text')
            ->add('description', 'text')
	        ->add('type', 'text')
	       	->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
        	$em = $this->getDoctrine()->getManager();
	        $em->persist($page);
	        $em->flush();
        	
        	return $this->redirect($this->generateUrl('dashboard'));
    	}
        return $this->render('MikadevPortfolioBundle:Content:add.html.twig', 
        	array('form' => $form->createView()));
    }

    public function updateAction(Request $request, $name)
    {
        if (false === $this->get('security.context')->isGranted('ROLE_ADMIN')) {
            return $this->redirect($this->generateUrl('oops'));
        }

    	$repository = $this->getDoctrine()
    					   ->getManager()
    					   ->getRepository('MikadevPortfolioBundle:Page');

    	$page = $repository->findByName($name);

        if (!$page) {
            throw $this->createNotFoundException("Cette page n'existe pas !");   
        }

    	$form = $this->createFormBuilder($page[0])
	        ->add('name', 'text')
            ->add('titre', 'text')
            ->add('soustitre', 'text')
	        ->add('content', 'textarea', array('required' => false))
	        ->add('logo', 'text')
            ->add('description', 'text')
	        ->add('type', 'text')
	       	->getForm();

    	$form->handleRequest($request);

    	if ($form->isValid()) {
    		$em = $this->getDoctrine()->getManager();
    		$em->persist($page[0]);
    		$em->flush();

    		return $this->redirect($this->generateUrl('dashboard'));
    	}
    	return $this->render('MikadevPortfolioBundle:Content:update.html.twig', 
        	array('form' => $form->createView()));
    }
}
