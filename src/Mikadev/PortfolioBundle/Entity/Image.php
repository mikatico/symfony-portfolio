<?php
namespace Mikadev\PortfolioBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Page
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="Mikadev\PortfolioBundle\Entity\ImageRepository")
 */
Class Image
{
 	/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="idPage", type="Integer")
     */
    private $idPage;

    /**
     * @var string
     *
     * @ORM\Column(name="nomImage", type="string", length=50)
     */
    private $nomImage;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPage
     *
     * @param string $idPage
     * @return Page
     */
    public function setIdPage($idPage)
    {
        $this->idPage = $idPage;

        return $this;
    }

    /**
     * Get idPage
     *
     * @return integer 
     */
    public function getIdPage()
    {
        return $this->idPage;
    }

    /**
     * Set nomImage
     *
     * @param string $nomImage
     * @return Page
     */
    public function setNomImage($nomImage)
    {
        $this->nomImage = $nomImage;

        return $this;
    }

    /**
     * Get nomImage
     *
     * @return integer 
     */
    public function getNomImage()
    {
        return $this->nomImage;
    }

}
